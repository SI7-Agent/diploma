from matplotlib import pyplot
import numpy as np
import matplotlib


def view():
    x = np.array([1, 5, 10, 25, 50, 100, 300, 500, 650, 850, 1000])
    y_const =    np.array([0.064, 0.091, 0.090, 0.136, 0.311, 0.488, 1.500, 2.552, 3.713, 4.923, 6.836])
    y_nonconst = np.array([0.070, 0.066, 0.061, 0.112, 0.253, 0.325, 0.904, 1.759, 2.677, 3.520, 3.854])
    # y_python =  np.array([0.003, 0.014, 0.031, 0.069, 0.120, 0.198, 0.664, 1.379, 2.167, 2.680, 4.042])
    # y_cpp =     np.array([0.001, 0.004, 0.011, 0.025, 0.042, 0.068, 0.181, 0.273, 0.464, 0.631, 0.813])
    # y_wrap =    np.array([0.001, 0.005, 0.009, 0.026, 0.045, 0.070, 0.195, 0.309, 0.591, 0.719, 0.950])
    # y_cpp_over_python =     np.array([y_python[i]/y_cpp[i]*100 for i in range(len(x))])
    # y_wrap_over_python =    np.array([y_python[i]/y_wrap[i]*100 for i in range(len(x))])

    pyplot.grid(True)
    const_reference, = pyplot.plot(x, y_const, "-r", label='Константная ссылка')
    non_const_reference, = pyplot.plot(x, y_nonconst, "-g", label='Неконстантная ссылка')
    pyplot.xlabel("Количество обработок")
    pyplot.ylabel("Среднее время обработки, c")
    # const_reference, = pyplot.plot(x, y_python, "-r", label='Модуль Python')
    # non_const_reference, = pyplot.plot(x, y_cpp, "-g", label='Модуль C++')
    # wrap, = pyplot.plot(x, y_wrap, "-y", label='Модуль C++ (Обернутый в Python-класс)')
    # cpp_over_python, = pyplot.plot(x, y_cpp_over_python, "-g", label='Модуль C++')
    # wrap_over_python, = pyplot.plot(x, y_wrap_over_python, "-y", label='Модуль C++ (Обернутый в Python-класс)')
    # pyplot.xlabel("Количество изображений")
    # pyplot.ylabel("Время обработки")
    pyplot.legend(handles=[const_reference, non_const_reference])
    # pyplot.legend(handles=[cpp_over_python, wrap_over_python])
    pyplot.show()


def view_resources():
    modules = np.array([1, 3, 5, 7, 9])
    images = np.array([1, 25, 100, 500, 850])

    recursive_modules_memory = np.array([107.6, 225.8, 278.9, 406.0, 415.4])
    recursive_modules_time = np.array([4.447, 4.887, 5.922, 15.491, 18.701])
    iterative_modules_memory = np.array([107.7, 227.7, 278.5, 396.5, 409.9])
    iterative_modules_time = np.array([4.786, 6.251, 6.689, 12.050, 16.709])

    recursive_images_memory = np.array([0, 0, 0, 0, 0])
    recursive_images_time = np.array([0, 0, 0, 0, 0])
    iterative_images_memory = np.array([0, 0, 0, 0, 0])
    iterative_images_time = np.array([0, 0, 0, 0, 0])

    recursive_modules_time_usage, = pyplot.plot(modules, recursive_modules_time, "-m", label='Рекурсивная реализация')
    iterative_modules_time_usage, = pyplot.plot(modules, iterative_modules_time, "-g", label='Итеративная реализация')

    pyplot.xlim([1, 9])
    pyplot.xticks(np.arange(1, 10, 2))
    pyplot.legend(handles=[recursive_modules_time_usage, iterative_modules_time_usage])
    pyplot.grid(True)
    pyplot.xlabel("Количество модулей")
    pyplot.ylabel("Среднее время работы, c")
    pyplot.show()

    recursive_modules_memory_usage, = pyplot.plot(modules, recursive_modules_memory, "-m", label='Рекурсивная реализация')
    iterative_modules_memory_usage, = pyplot.plot(modules, iterative_modules_memory, "-g", label='Итеративная реализация')

    pyplot.xlim([1, 9])
    pyplot.xticks(np.arange(1, 10, 2))
    pyplot.legend(handles=[recursive_modules_memory_usage, iterative_modules_memory_usage])
    pyplot.grid(True)
    pyplot.xlabel("Количество модулей")
    pyplot.ylabel("Среднее использование памяти, МБ")
    pyplot.show()


font = {'family': 'normal',
        'size': 14}

matplotlib.rc('font', **font)

view()
# view_resources()
