from exceptions.system_exceptions import NoConfigFile, ModuleImportError, MessagerImportError, NoMandatoryAttribute, TryPassConstReferenceToNonConstModule
from memory_profiler import profile
from modules.const_reference import Reference
from time import perf_counter

import commentjson
import os
import pprint
import psutil
import sys

from tools import is_const_ref_required


def print_memory(fn):
    def wrapper(*args, **kwargs):
        process = psutil.Process(os.getpid())
        meminfo = process.memory_info()
        start_rss, start_vms = meminfo[0], meminfo[1]
        try:
            return fn(*args, **kwargs)
        finally:
            meminfo = process.memory_info()
            end_rss, end_vms = meminfo[0], meminfo[1]
            print((end_rss - start_rss)/1048576, (end_vms - start_vms)/1048576)
    return wrapper


class Loader:
    __path = None
    __app_config = None
    __matrix = None

    def __init__(self, path):
        self.check_path(path)

    def const_modules(self):
        consts = []
        for i in self.__app_config["nodes"]:
            if is_const_ref_required(i):
                consts.append(i["name"])
        return consts

    def matrix(self):
        return self.__matrix

    # @profile
    def check_path(self, check_path):
        path = os.getcwd()

        if not os.path.exists(check_path):
            raise NoConfigFile(path + '\\' + check_path)

        self.__path = check_path

    # @profile
    def load_config(self):
        with open(self.__path, 'r') as config:
            read_config = commentjson.load(config)

        self.__app_config = read_config

    @staticmethod
    def __check_module_mandatory_attributes(module):
        mandatories = ['name', 'messager', 'path', 'class', 'attributes']

        for i in mandatories:
            if i not in module.keys():
                raise NoMandatoryAttribute(module['name'], i)

    # @profile
    def __import_single_module(self, module):
        self.__check_module_mandatory_attributes(module)

        try:
            module_import = __import__(module['path'].replace('/', '.'), fromlist=[module['name']])
        except:
            raise ModuleImportError(module['path'])

        imported_messager = self.__import_messager(module)

        if 'language' in module.keys() and module['language'] != 'python':
            imported_module = getattr(module_import, module['class'])
            wrapper = __import__('modules.module_wrapper', fromlist=['ModuleWrap'])
            import_wrapper = getattr(wrapper, 'ModuleWrap')
            imported_module_class = import_wrapper(imported_module, imported_messager())
        else:
            imported_module = getattr(module_import, module['class'])
            imported_module_class = imported_module(imported_messager())

        for key, value in module['attributes'].items():
            imported_module_class.set_config_attributes((key, value))

        return imported_module_class

    @staticmethod
    def __import_messager(module):
        try:
            if module['messager'] not in sys.modules:
                messager = __import__(module['messager'].replace('/', '.'), fromlist=[module['messager'].split('/')[-1].capitalize()])
                imported_messager = getattr(messager, module['messager'].split('/')[-1].capitalize())
            else:
                imported_messager = getattr(sys.modules[module['messager']], module['messager'].split('/')[-1].capitalize())
        except:
            raise MessagerImportError(module['messager'])

        return imported_messager

    # @profile
    def import_modules(self):
        modules = {}
        for i in self.__app_config['nodes']:
            modules[i['name']] = self.__import_single_module(i)

        return modules

    # @profile
    def link_modules(self):
        links = {}
        matrix = [[0 for i in range(len(self.__app_config['nodes']))]
                  for j in range(len(self.__app_config['nodes']))]

        for i in self.__app_config['nodes']:
            links[i['name']] = []

        for i in self.__app_config['edges']:
            links[i['from']].append(i['to'])
            matrix[[*links].index(i['from'])][[*links].index(i['to'])] = 1

        self.__matrix = matrix
        return links

    @staticmethod
    def create_reference(data, need_to_const):
        if need_to_const:
            if isinstance(data, Reference) and data.is_constant:
                data = Reference(data.data, False)
                data.was_constant = True
                return data
            if isinstance(data, Reference) and not data.is_constant:
                return data
            else:
                return Reference(data)
        else:
            if isinstance(data, Reference) and data.is_constant:
                raise TryPassConstReferenceToNonConstModule()
            elif isinstance(data, Reference) and not data.is_constant:
                if data.was_constant:
                    raise TryPassConstReferenceToNonConstModule()
                return data
            else:
                data = Reference(data, False)
                return data

    # @profile
    # @print_memory
    def compute_modules_pipeline_style(self, modules, links):
        pipelines = []

        def create_pipeline(current_module, visited):
            visited.append([*links][current_module])

            for i in range(len(self.__matrix[current_module])):
                if self.__matrix[current_module][i] == 1:
                    create_pipeline(i, visited.copy())

            if sum(self.__matrix[current_module]) == 0:
                pipelines.append(visited)

        start = [i if i[:5] == 'Input' else None for i in modules.keys()]
        start = list(filter(lambda a: a is not None, start))

        for i in start:
            create_pipeline([*links].index(i), [])
            for pipeline in pipelines:
                pipeline.pop(0)

            data = modules[i].process()  # Input returns start data
            for pipeline in pipelines:
                source_data = data
                # start = perf_counter()
                for module in pipeline:
                    print(module)
                    need_to_const = is_const_ref_required(self.__app_config['nodes'][[*links].index(module)])
                    source_data = self.create_reference(source_data, need_to_const)
                    # source_data = self.create_reference(modules[module].find(source_data), need_to_const)
                    source_data = modules[module].process(source_data)
                print()
                # duration = perf_counter() - start
                # print([i] + pipeline)
                # print('Took {:.3f} seconds\n\n'.format(duration))

            pipelines.clear()

    # @profile
    # @print_memory
    def compute_modules(self, modules, links):
        results = {}
        for i in self.__app_config['nodes']:
            results[i['name']] = []

        start = [i if i[:5] == 'Input' else None for i in modules.keys()]
        start = list(filter(lambda a: a is not None, start))

        for i in start:
            data = modules[i].process()
            stack = [([*links].index(i), None)]
            print(i, "\n")
            iteration = 0

            while len(stack) > 0:
                proc = stack.pop()
                cur_point = proc[0]
                prev_point = proc[1]
                for j in range(len(self.__matrix[cur_point])):
                    if self.__matrix[cur_point][j] == 1:
                        stack.append((j, cur_point))

                if iteration:
                    data = results[[*links][prev_point]]
                    need_to_const = is_const_ref_required(self.__app_config['nodes'][cur_point])
                    data = self.create_reference(data, need_to_const)

                    # data = self.create_reference(modules[[*links][cur_point]].find(data), need_to_const)
                    print([*links][cur_point])
                    data = modules[[*links][cur_point]].process(data)

                iteration += 1
                results[[*links][cur_point]] = data

            results.clear()
