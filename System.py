from time import perf_counter
from graph_creator import create_graph
import tracemalloc

from Loader import Loader

if __name__ == "__main__":
    # loader = Loader('app.json')
    # loader = Loader('check_const.json')
    # loader = Loader('check_const_error.json')
    loader = Loader('check_cpp.json')
    # loader = Loader('testing.json')
    loader.load_config()
    im = loader.import_modules()
    ml = loader.link_modules()

    # start = perf_counter()
    # tracemalloc.start()
    # loader.compute_modules(im, ml)
    # current, peak = tracemalloc.get_traced_memory()
    # print(f"Current memory usage is {current / 10**6}MB; Peak was {peak / 10**6}MB")
    # tracemalloc.stop()

    # tracemalloc.start()
    loader.compute_modules_pipeline_style(im, ml)
    # current, peak = tracemalloc.get_traced_memory()
    # print(f"Current memory usage is {current / 10**6}MB; Peak was {peak / 10**6}MB")
    # tracemalloc.stop()
    # duration = perf_counter() - start
    # print('took {:.3f} seconds\n\n'.format(duration))
    create_graph(im, ml, loader.const_modules())

