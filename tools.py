import cv2
import numpy as np
from modules.const_reference import Reference
from PIL import ImageFont, ImageDraw, Image


def is_immutable(data):
    set_of_immutable = [bool,
                        int,
                        float,
                        tuple,
                        str,
                        frozenset]

    attribute_of_mutations = True

    if True not in [isinstance(data, x) for x in set_of_immutable]:
        attribute_of_mutations = False

    if isinstance(data, Reference) and data.is_constant:
        attribute_of_mutations = True

    return attribute_of_mutations


def is_const_ref_required(module):
    required = False

    if 'input' in module.keys() and module['input'] == 'const':
        required = True

    return required


def show_cv_label(img, label):
    if label.isascii():
        cv2.putText(img, label, (0, 15), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 0), 2)
    else:
        font = ImageFont.truetype('times.ttf', 56)
        text_size = font.getsize(label)
        x = (img.shape[0] - text_size[0])//2
        y = (img.shape[1] - text_size[1])//2
        img_pil = Image.fromarray(img)
        draw = ImageDraw.Draw(img_pil)
        draw.text((x, y), label, font=font, fill=(0, 0, 0))
        img = np.array(img_pil)
    return img


def show_cv_rect(img, pos, color):
    cv2.rectangle(img, (pos["left"], pos["top"]), (pos["right"], pos["bottom"]), color, 2)
    return img
