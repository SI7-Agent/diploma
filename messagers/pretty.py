from messagers.messager import Messager


class Pretty(Messager):
    @staticmethod
    def transform_message(message):
        targets = []
        reference_value = message.data

        for i in reference_value:
            metadata = {
                "image": i["image"],
                "frame_number": 1
            }

            if "label" in i.keys():
                metadata["label"] = i["label"]

            if "location" in i.keys():
                metadata["location"] = i["location"]

            if "color" in i.keys():
                metadata["color"] = i["color"]

            targets.append(metadata)

        return targets
