from messagers.messager import Messager


class Multy(Messager):
    @staticmethod
    def transform_message(message):
        targets = []
        reference_value = message.data

        for i in reference_value:
            metadata = {
                "image": i["image"],
                "frame_number": i["frame_number"]
            }

            targets.append(metadata)

        return targets
