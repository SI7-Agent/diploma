from messagers.messager import Messager


class Single(Messager):
    @staticmethod
    def transform_message(message):
        reference_value = message.data

        if len(reference_value):
            data = [{
                    "image": reference_value[0]["image"],
                    "frame_number": 1
                    }]
        else:
            data = []

        return data
