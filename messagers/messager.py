from abc import ABC, abstractmethod


class Messager(ABC):
    @staticmethod
    @abstractmethod
    def transform_message(message):
        pass
