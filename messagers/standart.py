from messagers.messager import Messager


class Standart(Messager):
    @staticmethod
    def transform_message(message):
        targets = []
        reference_value = message.data

        for i in reference_value:
            metadata = {
                "image": i["image"],
                "frame_number": 1
            }

            targets.append(metadata)

        return targets
