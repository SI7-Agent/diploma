class VariableIsAlreadyDefined(Exception):
    """Exception raised for errors in the config variables.

    Attributes:
        variable -- variable which set caused the error
        value -- current variable value
        message -- explanation of the error
    """

    def __init__(self, variable, value, message):
        self.__variable = variable
        self.__value = value
        sample = "\nYou are trying to set already defined variable {:s} with current value {:s} by value {:s}"
        error_message = sample.format(self.__variable, self.__value, message)
        super().__init__(error_message)


class NoConfigFile(Exception):
    """Exception raised for errors with the config file.

    Attributes:
        path -- expected config path
    """

    def __init__(self, path):
        self.__path = path
        sample = "\nCan't find config file on {:s}"
        error_message = sample.format(self.__path)
        super().__init__(error_message)


class ModuleImportError(Exception):
    """Exception raised for errors with the detect module import.

    Attributes:
        module -- module with error
    """

    def __init__(self, module):
        self.__module = module
        sample = "\nCan't import module {:s}"
        error_message = sample.format(self.__module)
        super().__init__(error_message)


class MessagerImportError(Exception):
    """Exception raised for errors with the detect messager import.

    Attributes:
        messager -- messager with error
    """

    def __init__(self, messager):
        self.__messager = messager
        sample = "\nCan't import messager {:s}"
        error_message = sample.format(self.__messager)
        super().__init__(error_message)


class NoMandatoryAttribute(Exception):
    """Exception raised for errors when mandatory attribute is not found in config.

    Attributes:
        module -- module with error
        attribute -- lost attribute
    """

    def __init__(self, module, attribute):
        self.__module = module
        self.__attribute = attribute
        sample = "\nCan't find attribute {:s} for module {:s}"
        error_message = sample.format(self.__attribute, self.__module)
        super().__init__(error_message)


class TryConstRewrite(Exception):
    """Exception raised for errors when you try to change const reference variables.

    """

    def __init__(self):
        sample = "\nYou are trying to rewrite const reference variable"
        error_message = sample
        super().__init__(error_message)


class TryPassConstReferenceToNonConstModule(Exception):
    """Exception raised for errors when you try to change const reference variables.

    """

    def __init__(self):
        sample = "\nYou are trying to pass const reference variable to non const module"
        error_message = sample
        super().__init__(error_message)
