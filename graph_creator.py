import graphviz


def create_graph(nodes, edges, consts):
    dot = graphviz.Digraph(comment='Module Graph')

    for i in nodes.keys():
        dot.node(i, i)

    for i in edges.keys():
        for j in edges[i]:
            type = ''
            if j in consts:
                type = 'const'
            dot.edge(i, j, label=type)

    dot.render('generated_graph.gv', view=True)
