from exceptions.system_exceptions import VariableIsAlreadyDefined
from modules.module import Module

import cv2
import face_recognition
import numpy as np


class PeopleModule(Module):
    def __init__(self, msg):
        self._messager = msg
        self._color = [np.random.uniform(0, 255) for i in range(3)]
        self._model = face_recognition.face_locations
        self._config_attributes = {}

    def config_attributes(self):
        return self._config_attributes

    def set_config_attributes(self, value):
        variable = value[0]
        data = value[1]

        if variable in self._config_attributes.keys():
            raise VariableIsAlreadyDefined(variable, self._config_attributes[variable], data)

        self._config_attributes[variable] = data

    @staticmethod
    def get_face_from_image(face_location, small_frame):
        top, right, bottom, left = face_location
        face_image = small_frame[top:bottom, left:right]
        face_image = cv2.resize(face_image, (150, 150))

        return face_image

    def process(self, message):
        predictions = []
        message = self._messager.transform_message(message)
        for i in message:
            (h, w) = i["image"].shape[:2]
            if h <= 0 or w <= 0:
                continue
            small_frame = cv2.resize(i["image"], (0, 0), fx=0.25, fy=0.25)
            face_locations = self._model(small_frame)

            for (top, right, bottom, left) in face_locations:
                metadata = {
                     "label": "",
                     "color": None,
                     "image": None,
                     "location": None,
                     "frame_number": None
                }
                location = {
                     "left": None,
                     "top": None,
                     "right": None,
                     "bottom": None,
                }

                metadata["color"] = self._color
                metadata["image"] = self.get_face_from_image((top, right, bottom, left), small_frame)
                metadata["frame_number"] = i["frame_number"]

                top *= 4
                right *= 4
                bottom *= 4
                left *= 4

                location["left"] = left
                location["top"] = top
                location["right"] = right
                location["bottom"] = bottom

                metadata["location"] = location

                predictions.append(metadata)

        return predictions
