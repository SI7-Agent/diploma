from exceptions.system_exceptions import VariableIsAlreadyDefined
from modules.module import Module


class ModuleWrap(Module):
    def __init__(self, module_func_call, msg):
        self.__call = module_func_call
        self._messager = msg
        self._model = None
        self._config_attributes = {}

    @property
    def config_attributes(self):
        return self._config_attributes

    def set_config_attributes(self, value):
        variable = value[0]
        data = value[1]

        if variable in self._config_attributes.keys():
            raise VariableIsAlreadyDefined(variable, self._config_attributes[variable], data)

        self._config_attributes[variable] = data

    def process(self, message):
        predictions = []
        message = self._messager.transform_message(message)

        for i in message:
            metadata = self.__call(i)
            predictions.append(metadata)

        return predictions
