from exceptions.system_exceptions import VariableIsAlreadyDefined
from modules.module import Module

import cv2
import os
import numpy as np


class ObjectModule(Module):
    def __init__(self, msg):
        self._messager = msg
        self._classes = ["Background", "Aeroplane", "Bicycle",      "Bird",   "Boat",          "Bottle", "Bus",
                         "Car",        "Cat",       "Chair",        "Cow",    "Dinning table", "Dog",    "Horse",
                         "Motorbike",  "Person",    "Potted plant", "Sheep",  "Sofa",          "Train",  "Tv monitor"]
        self._colors = np.random.uniform(0, 255, size=(len(self._classes), 3))
        self._model = cv2.dnn.readNetFromCaffe(os.getcwd() + '\\modules\\objects\\obj_props.txt',
                                               os.getcwd() + '\\modules\\objects\\obj_mdl.caffemodel')
        self._config_attributes = {}

    def config_attributes(self):
        return self._config_attributes

    def set_config_attributes(self, value):
        variable = value[0]
        data = value[1]

        if variable in self._config_attributes.keys():
            raise VariableIsAlreadyDefined(variable, self._config_attributes[variable], data)

        self._config_attributes[variable] = data

    @staticmethod
    def get_object_from_image(object_location, frame):
        top, right, bottom, left = object_location
        object_image = frame[top:bottom, left:right]

        return object_image

    def process(self, message):
        predictions = []
        message = self._messager.transform_message(message)
        for i in message:
            (h, w) = i["image"].shape[:2]
            blob = cv2.dnn.blobFromImage(cv2.resize(i["image"], (300, 300)), 0.007843, (300, 300), 127.5)
            self._model.setInput(blob)
            detections = self._model.forward()
            for j in np.arange(0, detections.shape[2]):
                confidence = detections[0, 0, j, 2]

                if confidence > 0.2:
                    idx = int(detections[0, 0, j, 1])
                    if self._classes[idx] == "Person":
                        continue

                    metadata = {
                        "label": "",
                        "color": None,
                        "image": None,
                        "location": None,
                        "frame_number": None
                    }
                    location = {
                        "left": None,
                        "top": None,
                        "right": None,
                        "bottom": None,
                    }

                    box = detections[0, 0, j, 3:7] * np.array([w, h, w, h])
                    (startX, startY, endX, endY) = box.astype("int")

                    label = self._classes[idx]

                    location["left"] = int(startX)
                    location["top"] = int(startY)
                    location["right"] = int(endX)
                    location["bottom"] = int(endY)

                    metadata["label"] = label
                    metadata["location"] = location
                    metadata["color"] = self._colors[idx]
                    metadata["image"] = self.get_object_from_image((startY, endX, endY, startX), i["image"])
                    metadata["frame_number"] = i["frame_number"]

                    predictions.append(metadata)

        return predictions
