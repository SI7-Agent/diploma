from abc import ABC, abstractmethod


class Module(ABC):
    @abstractmethod
    def config_attributes(self):
        pass

    @abstractmethod
    def set_config_attributes(self, to_set):
        pass

    @abstractmethod
    def process(self, message):
        pass
