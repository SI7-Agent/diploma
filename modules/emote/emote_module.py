from exceptions.system_exceptions import VariableIsAlreadyDefined
from modules.module import Module

import cv2
import keras
import keras.preprocessing.image as preimage
import numpy as np
import os


class EmoteModule(Module):
    def __init__(self, msg):
        self._messager = msg
        self._emotes = ["Angry", "Disgust", "Scared", "Happy", "Sad", "Surprised", "Neutral"]
        self._model = keras.models.load_model(os.getcwd() + '\\modules\\emotes\\emt_mdl.hdf5')
        self._config_attributes = {}

    def config_attributes(self):
        return self._config_attributes

    def set_config_attributes(self, value):
        variable = value[0]
        data = value[1]

        if variable in self._config_attributes.keys():
            raise VariableIsAlreadyDefined(variable, self._config_attributes[variable], data)

        self._config_attributes[variable] = data

    def process(self, message):
        predictions = []
        message = self._messager.transform_message(message)
        for i in message:
            metadata = {
                "label": "",
                "color": None,
                "image": None,
                "location": None,
                "frame_number": None
            }

            roi = cv2.cvtColor(i["image"], cv2.COLOR_BGR2GRAY)
            roi = cv2.resize(roi, (64, 64))
            roi = roi.astype("float") / 255.0
            roi = preimage.img_to_array(roi)
            roi = np.expand_dims(roi, axis=0)

            predicted = self._model.predict(roi)
            predicted_class = np.argmax(predicted[0])

            metadata["label"] = self._emotes[int(predicted_class)]
            metadata["image"] = i["image"]
            metadata["frame_number"] = i["frame_number"]

            predictions.append(metadata)

        return predictions
