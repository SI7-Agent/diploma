from copy import deepcopy
from exceptions.system_exceptions import TryConstRewrite


class Reference:
    def __init__(self, data, is_constant=True):
        self.__data = data
        self.__is_constant = is_constant
        self.__was_constant = False

    @property
    def data(self):
        if self.__is_constant:
            return deepcopy(self.__data)
        else:
            return self.__data

    @data.setter
    def data(self, new_data):
        if self.__is_constant:
            raise TryConstRewrite()

        self.__data = new_data

    @property
    def is_constant(self):
        return self.__is_constant

    @property
    def was_constant(self):
        return self.__was_constant

    @was_constant.setter
    def was_constant(self, new_was_constant):
        self.__was_constant = new_was_constant
