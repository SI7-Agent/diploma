#define NOMINMAX

#include <Windows.h>
#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>

namespace py = pybind11;

py::dict process(py::dict& message)
{
    py::dict metadata;
    metadata[py::str("label")] = py::none();
    metadata[py::str("image")] = py::none();
    metadata[py::str("color")] = py::none();
    metadata[py::str("location")] = py::none();
    metadata[py::str("frame_number")] = py::none();

    py::array_t<uint8_t> img = message["image"].cast<py::array_t<uint8_t>>();

    auto rows = img.shape(0);
    auto cols = img.shape(1);

    std::string returnal = "Passed picture with width: ";
    returnal += std::to_string(rows);
    returnal += std::string(" and height: ");
    returnal += std::to_string(cols);

    metadata[py::str("label")] = returnal;
    metadata[py::str("frame_number")] = message[py::str("frame_number")];

    return metadata;
}

py::dict process2(py::dict& message)
{
    py::dict metadata;
    metadata[py::str("label")] = py::none();
    metadata[py::str("image")] = py::none();
    metadata[py::str("color")] = py::none();
    metadata[py::str("location")] = py::none();
    metadata[py::str("frame_number")] = py::none();

    py::array_t<uint8_t> img = message["image"].cast<py::array_t<uint8_t>>();
    auto rows = img.shape(0);
    auto cols = img.shape(1);

    std::string returnal = "Passed picture with width: ";
    returnal += std::to_string(rows);
    returnal += std::string(" and height: ");
    returnal += std::to_string(cols);

    metadata[py::str("label")] = returnal;
    metadata[py::str("frame_number")] = message[py::str("frame_number")];

    py::buffer_info buf1 = img.request();
    uint8_t* ptr1 = (uint8_t*)buf1.ptr;

    int i = 239;
    for (int j = 0; j < buf1.shape[1]; j++)
    {
        for (int k = 0; k < buf1.shape[2]; k++)
        {
            auto value1 = ptr1[i * buf1.shape[1] * buf1.shape[2] + j * buf1.shape[2] + k];
        }
    }

    return metadata;
}

PYBIND11_MODULE(demo_module, m)
{
    m.def("process", &process, R"pbdoc(Demo module for demonstration of passing data to c++ module and returning some data to python.)pbdoc");
    m.def("process2", &process2, R"pbdoc(Demo module for demonstration of data access c++ speed in comparison with python.)pbdoc");

#ifdef VERSION_INFO
    m.attr("__version__") = VERSION_INFO;
#else
    m.attr("__version__") = "dev";
#endif
}