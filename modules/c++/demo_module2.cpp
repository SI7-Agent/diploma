#define NOMINMAX

#include <iostream>
#include <Windows.h>
#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>

namespace py = pybind11;

class VariableIsAlreadyDefined : public std::exception
{
public:
    explicit VariableIsAlreadyDefined(const char* var, const char* val, const char* msg) :
        __variable{ var },
        __value{ val },
        __message{ msg }
    {
        __error_message += "You are trying to set already defined variable '";
        __error_message += __variable;
        __error_message += std::string("' with current value '");
        __error_message += __value;
        __error_message += std::string("' by value '");
        __error_message += __message;
        __error_message += std::string("'");
    }

    const char* what() const noexcept override
    {
        return __error_message.c_str();
    }

private:
    std::string __variable = "";
    std::string __value = "";
    std::string __message = "";
    std::string __error_message = "";
};

class DemoModuleCPP
{
protected:
    void* _model;
    py::dict _config_attributes;

public:
    py::object messager;

    DemoModuleCPP(py::object msg)
    {
        this->messager = msg;
        this->_model = nullptr;
    }

    void set_config_attributes(py::tuple values)
    {
        std::string key = values[0].cast<std::string>();
        std::string value = values[1].cast<std::string>();

        for (auto it : this->_config_attributes)
            if (it.first.cast<std::string>() == key)
                throw VariableIsAlreadyDefined(key.c_str(), it.second.cast<std::string>().c_str(), value.c_str());

        this->_config_attributes[py::str(key)] = py::str(value);
    }

    py::dict config_attributes()
    {
        return this->_config_attributes;
    }

    py::list process(py::object& message)
    {
        py::list predictions;
        py::list frame = this->messager.attr("transform_message")(message);

        for (auto it : frame)
        {
            py::dict metadata;
            metadata[py::str("label")] = py::none();
            metadata[py::str("image")] = py::none();
            metadata[py::str("color")] = py::none();
            metadata[py::str("location")] = py::none();
            metadata[py::str("frame_number")] = py::none();

            py::array_t<uint8_t> img = it.cast<py::dict>()["image"].cast<py::array_t<uint8_t>>();
            auto rows = img.shape(0);
            auto cols = img.shape(1);

            std::string returnal = "Passed picture with width: ";
            returnal += std::to_string(rows);
            returnal += std::string(" and height: ");
            returnal += std::to_string(cols);

            metadata[py::str("label")] = returnal;
            metadata[py::str("frame_number")] = it[py::str("frame_number")];

            /*py::buffer_info buf1 = img.request();
            uint8_t* ptr1 = (uint8_t*)buf1.ptr;
            
            int i = 239;
            for (int j = 0; j < buf1.shape[1]; j++)
            {
                for (int k = 0; k < buf1.shape[2]; k++)
                {
                    auto value1 = ptr1[i*buf1.shape[1]* buf1.shape[2] + j*buf1.shape[2] + k];
                }
            }*/

            predictions.append(metadata);
        }

        return predictions;
    }
};

PYBIND11_MODULE(demo_module2, m)
{
    static py::exception<VariableIsAlreadyDefined> ex(m, "VariableIsAlreadyDefined");
    py::register_exception_translator([](std::exception_ptr p)
    {
        try
        {
            if (p)
                std::rethrow_exception(p);
        }
        catch (const VariableIsAlreadyDefined& e)
        {
            ex(e.what());
        }
    });

    py::class_<DemoModuleCPP>(m, "DemoModuleCPP", R"pbdoc(Demo module for demonstration of passing data to c++ module and returning some data to python.)pbdoc")
        .def(py::init<py::object>(), R"pbdoc(Demo module constructor.)pbdoc")
        .def_readwrite("messager", &DemoModuleCPP::messager)
        .def("config_attributes", &DemoModuleCPP::config_attributes, R"pbdoc(Demo module config variables.)pbdoc")
        .def("set_config_attributes", &DemoModuleCPP::set_config_attributes, R"pbdoc(Demo module set config variables.)pbdoc")
        .def("process", &DemoModuleCPP::process, R"pbdoc(Demo module processing method.)pbdoc");

#ifdef VERSION_INFO
    m.attr("__version__") = VERSION_INFO;
#else
    m.attr("__version__") = "dev";
#endif
}