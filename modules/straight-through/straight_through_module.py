from exceptions.system_exceptions import VariableIsAlreadyDefined
from modules.module import Module


class StraightThroughModule(Module):
    def __init__(self, msg):
        self._messager = msg
        self._config_attributes = {}

    def config_attributes(self):
        return self._config_attributes

    def set_config_attributes(self, value):
        variable = value[0]
        data = value[1]

        if variable in self._config_attributes.keys():
            raise VariableIsAlreadyDefined(variable, self._config_attributes[variable], data)

        self._config_attributes[variable] = data

    def process(self, message):
        return message
