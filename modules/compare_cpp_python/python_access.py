from exceptions.system_exceptions import VariableIsAlreadyDefined
from modules.module import Module


class PythonAccess(Module):
    def __init__(self, msg):
        self._messager = msg
        self._config_attributes = {}

    def config_attributes(self):
        return self._config_attributes

    def set_config_attributes(self, value):
        variable = value[0]
        data = value[1]

        if variable in self._config_attributes.keys():
            raise VariableIsAlreadyDefined(variable, self._config_attributes[variable], data)

        self._config_attributes[variable] = data

    def process(self, message):
        predictions = []
        message = self._messager.transform_message(message)

        for i in message:
            metadata = {
                "label": None,
                "color": None,
                "image": None,
                "location": None,
                "frame_number": None
            }

            img = i["image"]
            (rows, cols) = i["image"].shape[:2]

            returnal = "Passed picture with width: "
            returnal += str(rows)
            returnal += " and height: "
            returnal += str(cols)

            metadata["label"] = returnal
            metadata["frame_number"] = i["frame_number"]

            i = 239
            for j in range(0, cols):
                for k in range(0, 3):
                    value1 = img[i][j][k]

            predictions.append(metadata)

        return predictions
