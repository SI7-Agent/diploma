from exceptions.system_exceptions import VariableIsAlreadyDefined
from modules.module import Module

import cv2
import os


class GenderModule(Module):
    def __init__(self, msg):
        self._messager = msg
        self._model = cv2.dnn.readNetFromCaffe(os.getcwd() + '\\modules\\gender\\gnd_props.prototxt',
                                               os.getcwd() + '\\modules\\gender\\gnd_mdl.caffemodel')
        self._config_attributes = {}

    def config_attributes(self):
        return self._config_attributes

    def set_config_attributes(self, value):
        variable = value[0]
        data = value[1]

        if variable in self._config_attributes.keys():
            raise VariableIsAlreadyDefined(variable, self._config_attributes[variable], data)

        self._config_attributes[variable] = data

    def process(self, message):
        predictions = []
        message = self._messager.transform_message(message)
        for i in message:
            metadata = {
                "label": "",
                "color": None,
                "image": None,
                "location": None,
                "frame_number": None
            }

            blob = cv2.dnn.blobFromImage(i["image"], 1, (227, 227), (78.4263377603, 87.7689143744, 114.895847746), swapRB=False)
            self._model.setInput(blob)
            gender_prediction = self._model.forward()
            gender = gender_prediction[0].argmax()

            # metadata["label"] = "Woman" if int(gender) else "Man"
            metadata["label"] = "\u2640" if int(gender) else "\u2642"
            metadata["image"] = i["image"]
            metadata["frame_number"] = i["frame_number"]

            predictions.append(metadata)

        return predictions
