import cv2
from exceptions.system_exceptions import VariableIsAlreadyDefined
from modules.module import Module
from tools import show_cv_label


class OpenCVOutputModule(Module):
    def __init__(self, msg):
        self._messager = msg
        self._config_attributes = {}

    def config_attributes(self):
        return self._config_attributes

    def set_config_attributes(self, value):
        variable = value[0]
        data = value[1]

        if variable in self._config_attributes.keys():
            raise VariableIsAlreadyDefined(variable, self._config_attributes[variable], data)

        self._config_attributes[variable] = data

    def process(self, message):
        message = self._messager.transform_message(message)
        for i in message:
            i["image"] = show_cv_label(i["image"], i["label"])
            cv2.imshow('Image', i["image"])
            if cv2.waitKey(1) & 0xFF == ord('q'):
                continue
            cv2.waitKey(0)

        return None
