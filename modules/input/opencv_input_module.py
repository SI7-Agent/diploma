from exceptions.system_exceptions import VariableIsAlreadyDefined
from modules.module import Module
from sys import platform

import cv2


class OpenCVInputModule(Module):
    def __init__(self, msg):
        self._messager = msg
        self._config_attributes = {}
        self._camera = None

    def config_attributes(self):
        return self._config_attributes

    def set_config_attributes(self, value):
        variable = value[0]
        data = value[1]

        if variable in self._config_attributes.keys():
            raise VariableIsAlreadyDefined(variable, self._config_attributes[variable], data)

        self._config_attributes[variable] = data

    def process(self, message=0):
        predictions = []
        if self._camera is None:
            if message == 'def':
                if platform == "linux" or platform == "linux2":
                    self._camera = cv2.VideoCapture(-1)
                elif platform == "win32":
                    self._camera = cv2.VideoCapture(0)
            else:
                self._camera = cv2.VideoCapture(int(message))

        if 'timing' not in self._config_attributes.keys():
            timing = 1
        else:
            timing = int(self._config_attributes["timing"])

        for i in range(1, timing + 1):
            metadata = {
                "label": "",
                "color": None,
                "image": None,
                "location": None,
                "frame_number": None
            }

            ret, frame = self._camera.read()
            frame = cv2.flip(frame, 1)

            metadata["image"] = frame
            metadata["frame_number"] = i

            predictions.append(metadata)

        return predictions
